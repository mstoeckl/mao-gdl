<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="generic/template.xsl"/>
    <xsl:import href="generic/state.xsl"/>
    <xsl:import href="generic/sitespecific.xsl"/>
    
    <xsl:template name="print_state">
        
        <xsl:variable name="magic" select="fact[prop-f='HOLDS']"/>

        <style type="text/css" media="all">
            div.board {
                position:relative;
                width: 720px;
                height: 96px;
                padding: 0px;
            }
            div.piece {
                position: absolute;
                padding:  0px;
            }
            div.occu {
                position: absolute;
                padding: 0px;
            }
        </style>
        
        <xsl:variable name="active" select="fact[prop-f='TURNSTATE']/arg[1]"/>
        <xsl:variable name="nxtpl" select="fact[prop-f='TURNSTATE']/arg[2]"/>
        <xsl:variable name="lastcard" select="fact[prop-f='LAST-CARD']/arg"/>
        <xsl:variable name="postmove" select="fact[prop-f='HAS-MOVED']"/>

        <div>Current player: <xsl:value-of select="$active"/> </div>
        <div>Next player: <xsl:value-of select="fact[prop-f='TURNSTATE']/arg[2]"/> </div>
        <div>Last card: <xsl:value-of select="$lastcard"/> </div>
        <img>
            <xsl:attribute name="src"><xsl:value-of select="concat($stylesheetURL, '/cards/', translate($lastcard, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '.png')"/></xsl:attribute>
        </img>
        
        <div><b>Hands</b></div>

        <!-- Draw Board -->
        <xsl:for-each select="../role">
            <xsl:sort select="."/>
            <xsl:variable name="mplayer" select="translate(current(),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
            <xsl:if test="$mplayer != 'RANDOM'">
                <xsl:call-template name="show_deck">
                    <xsl:with-param name="mplayer" select="$mplayer"/>
                    <xsl:with-param name="magic" select="$magic"/>
                    <xsl:with-param name="active" select="$active"/>
                    <xsl:with-param name="nxtpl" select="$nxtpl"/>
                    <xsl:with-param name="hasmoved" select="$postmove"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:for-each>
        
        <!-- show remaining fluents -->
        <xsl:call-template name="state">
            <xsl:with-param name="excludeFluent" select="'HOLDS'"/>
<!--             <xsl:with-param name="excludeFluent2" select="'LAST-CARD'"/> -->
            <xsl:with-param name="excludeFluent3" select="'RULE'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template name="show_deck">
        <xsl:param name = "mplayer" />
        <xsl:param name = "magic" />
        <xsl:param name = "active" />
        <xsl:param name = "nxtpl" />
        <xsl:param name = "hasmoved" />

        <xsl:choose>
         <xsl:when test="$active=$mplayer and $hasmoved">
            <div style="font-weight: bold; color: blue;">Hand of: <xsl:value-of select="$mplayer"/></div>
         </xsl:when>
         <xsl:when test="$active=$mplayer">
            <div style="font-weight: bold; color: red;">Hand of: <xsl:value-of select="$mplayer"/></div>
         </xsl:when>
         <xsl:when test="$nxtpl=$mplayer">
            <div style="font-weight: normal; color: green;">Hand of: <xsl:value-of select="$mplayer"/></div>
         </xsl:when>
         <xsl:otherwise>
            <div>Hand of: <xsl:value-of select="$mplayer"/></div>
         </xsl:otherwise>
        </xsl:choose>

        <div class="board">
            <img>
                <xsl:attribute name="src"><xsl:value-of select="concat($stylesheetURL,'/board.png')"/></xsl:attribute>
            </img>
            <xsl:for-each select="$magic[arg[1]=$mplayer]">
                <xsl:sort select="./arg[2]"/>
                <xsl:variable name="player" select="./arg[1]"/>
                <xsl:variable name="card" select="arg[2]"/>
                <xsl:variable name="id" select="arg[3]"/>
                <xsl:variable name="i" select="position()" />

                <div class="occu">
                    <xsl:attribute name="style">
                        left: <xsl:value-of select="-72+($i * 72)"/>px;
                        top: <xsl:value-of select="0"/>px;
                    </xsl:attribute>
                    <img>
                        <xsl:attribute name="src"><xsl:value-of select="concat($stylesheetURL, '/cards/', translate($card, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '.png')"/></xsl:attribute>
                    </img>
                </div>
            </xsl:for-each>
        </div>
    </xsl:template>
</xsl:stylesheet>


