<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
	- Widget for writing move history to the screen.
	- For use within <body>.
	- needs css/main.css and sitespecific.xsl
	
	TODO: make height dynamic
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="variables.xsl"/>

	<xsl:template name="history">
		
		<div class="history">

			<span class="heading">History: </span>
			<div class="underline"/>
			
			<style type="text/css" media="all">
				td:last-child {padding-right: 20px;} /*prevent Mozilla scrollbar from hiding cell content*/
			</style>
			
			<div style="overflow-x: visible; max-height: 420px; overflow-y: scroll;">
				<table>
                    <xsl:variable name="roles" select="match"/>
					<thead>
						<tr>
							<td></td>
							<xsl:for-each select="match/role">
                                <xsl:sort select="."/>
								<th>
									<span class="heading">
										<xsl:value-of select="."/>
									</span>
								</th>
							</xsl:for-each>
						</tr>
					</thead>
					<tbody>
						<xsl:for-each select="match/history/step">
							<xsl:sort select="number(step-number)" order="descending" data-type="number"/>
							<tr>
								<td>
									<span class="heading">
									<a>
										<xsl:attribute name="href">
											<xsl:call-template name="makeStepLinkURL">
												<xsl:with-param name="step" select="./step-number"/>
												<xsl:with-param name="role" select="$role"/>
											</xsl:call-template>
										</xsl:attribute>
										<xsl:value-of select="./step-number"/>.
									</a>
									</span>
								</td>
								<xsl:variable name="move" select="."/>
								<xsl:for-each select="$roles/role">
                                    <xsl:sort select="."/>
                                    <xsl:variable name="i" select="position()"/>
                                    <xsl:variable name="v" select="current()"/>
									<td>
										<span class="content">
										<xsl:for-each select="$roles/role">
                                            <xsl:if test="current()=$v">
                                                <xsl:variable name="j" select="position()"/>
                                                <xsl:variable name="act" select="$move/move[$j]"/>
                                                <xsl:choose>
                                                    <xsl:when test="contains($act,'NOOP')">
                                                        . . .
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$act"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:if>
										</xsl:for-each>
										</span>
									</td>
								</xsl:for-each>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</div>

		</div>

	</xsl:template>
</xsl:stylesheet>

