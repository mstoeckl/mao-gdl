# GDL formalization for Mao

To run: you should copy/link `gamecontroller-cli.jar` from the GGPServer [1] 
into the same directory as the run script. Then, calling

    ./run

should suffice to run a match of Mao with random players.

# 3rd party materials

Uses the GGPServer (see [1]), licensed under the GPLv2 (see [2]). Because 
serveral modifications to the provided XSL templates were necessary for this 
specific game, they are included in this project under generic/.

The games Mastermind, Small Dominion, Stratego, and Snake are included for 
comparison testing. All come from [4].

For the visualization, the playing cards from [3] are included under mao/cards.

[1] https://sourceforge.net/projects/ggpserver/
[2] http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
[3] https://web.archive.org/web/20150814180133/http://jfitz.com/cards/index.html
[4] http://ggpserver.general-game-playing.de/ggpserver/public/show_games.jsp
